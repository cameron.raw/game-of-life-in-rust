use crate::cellstate::CellState;

struct Cell {
    state: CellState
}

impl Cell {
    fn get_state(&self) -> &CellState{
        &self.state
    }
    fn get_next_status(&mut self, no_of_live_neighbours: i64) -> (){
        if no_of_live_neighbours < 2 || no_of_live_neighbours > 3 {
            self.state = CellState::DEAD;
        } else {
            self.state = CellState::ALIVE;
        }
    }
} 

#[cfg(test)]
mod cell_should {
    #[test]
    fn be_initialized_with_live_state(){
        use super::*;
        let cell = Cell {
            state : CellState::ALIVE
        };
        assert!(&CellState::ALIVE == cell.get_state());
    }
    #[test]
    fn be_initialized_with_dead_state(){
        use super::*;
        let cell = Cell {
            state : CellState::DEAD
        };
        assert!(&CellState::DEAD == cell.get_state());
    }
}

#[cfg(test)]
mod live_cell_should {
    #[test]
    fn die_if_has_fewer_than_two_live_neighbours(){
        use super::*;
        let mut live_cell = Cell {
            state : CellState::ALIVE
        };
        live_cell.get_next_status(1);
        assert!(&CellState::DEAD == live_cell.get_state());
    }
    #[test]
    fn survive_if_has_two_live_neighbours(){
        use super::*;
        let mut live_cell = Cell {
            state : CellState::ALIVE
        };
        live_cell.get_next_status(2);
        assert!(&CellState::ALIVE == live_cell.get_state());
    }
    #[test]
    fn survive_if_has_three_live_neighbours(){
        use super::*;
        let mut live_cell = Cell {
            state : CellState::ALIVE
        };
        live_cell.get_next_status(3);
        assert!(&CellState::ALIVE == live_cell.get_state());
    }
    #[test]
    fn die_if_has_more_than_three_live_neighbours(){
        use super::*;
        let mut live_cell = Cell {
            state : CellState::ALIVE
        };
        live_cell.get_next_status(4);
        assert!(&CellState::DEAD == live_cell.get_state());
    }
}

#[cfg(test)]
mod dead_cell_should {
    #[test]
    fn revive_if_has_three_live_neighbours(){
        use super::*;
        let mut dead_cell = Cell {
            state : CellState::DEAD
        };
        dead_cell.get_next_status(3);
        assert!(&CellState::ALIVE == dead_cell.get_state());
    }
}
