#[derive(PartialEq, Debug)]
pub enum CellState {
    ALIVE,
    DEAD
}
